# You will be able to

1. Explain what an **array** is and why they're used
2. Use and manipulate Ruby’s core **data structures**

# Data Structures

In this lesson you'll be learning about one of Ruby's core data structures: **arrays**. Data structures are containers that store multiple pieces of data at once. They allow you to do in a few lines of code what would have taken you hundreds of lines to do before. Think of an array like a big list of pieces of information, like all the tweets you've ever tweeted, all the photos you've posted on Instagram, all the photos in an album on Facebook.
